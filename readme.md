# Open Repo Manager

## Welcome to our GitLab Repository

Maintaining a functional APT repo is hard enough. Launching a package management site for it is even worse. There are very few platforms out there capable of building a repository browser that actually _looks_ good, let alone works properly. Open Repo Manager is an attempt to change that. Leveraging the flexibility of WordPress, you can launch your own APT repo with ease!

### Installation

1. You can clone the GitLab repository: `https://gitlab.com/open-repo-manager/Open-Repo-Manager.git`
2. Or download it directly as a ZIP file: `https://gitlab.com/open-repo-manager/Open-Repo-Manager/-/archive/master/Open-Repo-Manager-master.zip`

This will download the latest developer copy of Open Repo Manager.

### Bugs

If you find an issue, let us know [here](https://gitlab.com/open-repo-manager/Open-Repo-Manager/issues?state=open)!
