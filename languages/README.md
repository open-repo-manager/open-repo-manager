# Open Repo Manager

This folder contains translation files for Open Repo Manager.

Do not store custom translations in this folder, they will be deleted on updates.
Store custom translations in `wp-content/languages/repo-manager`.
