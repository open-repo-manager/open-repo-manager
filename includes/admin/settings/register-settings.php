<?php
/**
 * Register settings
 *
 * @package     Repo_Manager\Admin\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function repo_manager_add_menu( $menu ) {
	$menu['page_title'] = __( 'Open Repo Manager Settings', 'repo-manager' );
	$menu['menu_title'] = __( 'Repo Manager', 'repo-manager' );
	$menu['icon']       = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmcyIgogICB4bWw6c3BhY2U9InByZXNlcnZlIgogICB3aWR0aD0iNzQiCiAgIGhlaWdodD0iNzQuMDAwOTc3IgogICB2aWV3Qm94PSIwIDAgNzQgNzQuMDAwOTc1IgogICBzb2RpcG9kaTpkb2NuYW1lPSJpY29uLnN2ZyIKICAgaW5rc2NhcGU6ZXhwb3J0LWZpbGVuYW1lPSIvaG9tZS9kZ3JpZmZpdGhzL0Rvd25sb2Fkcy9sb2dvLnBuZyIKICAgaW5rc2NhcGU6ZXhwb3J0LXhkcGk9IjEzMC4zNCIKICAgaW5rc2NhcGU6ZXhwb3J0LXlkcGk9IjEzMC4zNCIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSI+PG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhOCI+PHJkZjpSREY+PGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPjxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PjxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz48ZGM6dGl0bGU+PC9kYzp0aXRsZT48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGRlZnMKICAgICBpZD0iZGVmczYiIC8+PHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxODU4IgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwMTYiCiAgICAgaWQ9Im5hbWVkdmlldzQiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGZpdC1tYXJnaW4tdG9wPSI1IgogICAgIGZpdC1tYXJnaW4tbGVmdD0iNSIKICAgICBmaXQtbWFyZ2luLXJpZ2h0PSI1IgogICAgIGZpdC1tYXJnaW4tYm90dG9tPSI1IgogICAgIGlua3NjYXBlOnpvb209IjEuNjkzODM5MyIKICAgICBpbmtzY2FwZTpjeD0iNDUuOTQxMjg5IgogICAgIGlua3NjYXBlOmN5PSIxOC4xMDk4MDkiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjYyIgogICAgIGlua3NjYXBlOndpbmRvdy15PSIyNyIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9ImcxMiIgLz48ZwogICAgIGlkPSJnMTAiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpbmtzY2FwZTpsYWJlbD0iaW5rX2V4dF9YWFhYWFgiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMS4zMzMzMzMzLDAsMCwtMS4zMzMzMzMzLC01NS40NTMxNTIsMTIxLjQzMTE1KSI+PGcKICAgICAgIGlkPSJnMTIiCiAgICAgICB0cmFuc2Zvcm09InNjYWxlKDAuMSkiPjxwYXRoCiAgICAgICAgIGQ9Ik0gODU0Ljg4NjQ4LDYzMy4yMTgyMiBIIDcyNi4wMjk1MyB2IC03OC4wOTg3OSBoIDE5Ljc2NTM3IGwgLTM2LjE1NzcxLC02Mi42MTIxNyAtMzYuMTQ1NSw2Mi42MTIxNyBoIDE5Ljc0NjY1IHYgMTU3LjkxOTQ5IGggMjAuMDc2MjMgbCAtMzYuMTU0NDUsNjIuNTg5MzggLTM2LjE0ODc1LC02Mi41ODkzOCBoIDE5Ljc1MjM0IHYgLTc5LjgyMDcgSCA1MzEuOTA0MzMgYyAwLDg5LjE5ODQ1IDcyLjI5NjcsMTYxLjQ4OTQ1IDE2MS40OTI3MSwxNjEuNDg5NDUgODkuMTg4NjgsMCAxNjEuNDg5NDUsLTcyLjI5MSAxNjEuNDg5NDQsLTE2MS40ODk0NSB6IE0gNjY2LjY4OTQ5LDg2Ni42OTEwNyA2NjMuMjU5NSw4NDQuMDI3OTEgYyAtMTcuMTM2OTMsLTIuNDE2ODYgLTMzLjYwNTc2LC02LjkwNjM2IC00OS4xNzg2NCwtMTMuMTY4MjMgbCAtMTQuMjg4NzcsMTcuOTA0MyBjIC0yLjI1NjU1LDIuODMxODggLTcuOTU5MzgsMi45Mzc2NyAtMTIuNjc5OTgsMC4yMTk3MiBsIC0yNy40MjkzNCwtMTUuODM1NzMgYyAtNC43MTA4NCwtMi43MTc5NSAtNy40NzI3NSwtNy43MDg3MyAtNi4xNTAzOSwtMTEuMDg3NDUgbCA4LjM4MjUyLC0yMS4zNjc2NiBjIC0xMy4zNzk4MSwtMTAuNTMwODQgLTI1LjQ2MTY3LC0yMi42MDA1IC0zNS45ODAzMSwtMzUuOTgzNTYgbCAtMjEuMzc0MTcsOC4zODI1MiBjIC0zLjM3ODcyLDEuMzI4ODYgLTguMzYyOTksLTEuNDUyNTYgLTExLjA5MDcsLTYuMTY2NjYgbCAtMTUuODMzMjksLTI3LjQwNzM3IGMgLTIuNzIxMjEsLTQuNzE0MSAtMi42MjE5MywtMTAuNDM1NjMgMC4yMTMyLC0xMi42ODMyMyBsIDE3LjkwMjY4LC0xNC4yOTc3MyBDIDQ4OS40OTYxNCw2OTYuOTYxNTEgNDg1LjAxODAzLDY4MC40ODg2IDQ4Mi41ODU3LDY2My4zNDU5OCBsIC0yMi42NTgyNywtMy40MjM0OCBjIC0zLjU4ODY3LC0wLjUyNDg3IC02LjUyODc4LC01LjQxOTYzIC02LjUyODc4LC0xMC44NjUzIHYgLTMxLjY3MzA4IGMgMCwtNS40Mjg1OCAyLjk0MDExLC0xMC4zMzA2NiA2LjUyODc4LC0xMC44NzEgbCAyMi42NTgyNywtMy40MjY3MyBjIDIuNDMyMzMsLTE3LjE0MjYzIDYuOTEwNDQsLTMzLjYxMjI3IDEzLjE2NjYxLC00OS4xOTA4NiBsIC0xNy45MDI2OCwtMTQuMjgyMjYgYyAtMi44MzUxMywtMi4yNTAwNCAtMi45MzExNSwtNy45NjU4OCAtMC4yMDk5NSwtMTIuNjc5OTggbCAxNS44MzAwNCwtMjcuNDI2MDggYyAyLjcxNzk1LC00LjcxNDEgNy43MTE5OCwtNy40NzAzIDExLjA5MDcsLTYuMTQ3OTQgbCAyMS4zNzQxNyw4LjM4MjUyIGMgMTAuNTE4NjQsLTEzLjM4NTUxIDIyLjYwMDUsLTI1LjQ2ODE5IDM1Ljk4MDMxLC0zNS45ODY4MiBsIC04LjM4MjUyLC0yMS4zNjM1OSBjIC0xLjMyNTYxLC0zLjM4Mjc5IDEuNDM5NTQsLTguMzc2MDEgNi4xNjAxNiwtMTEuMDkzOTYgbCAyNy40MTcxMiwtMTUuODMwMDQgYyA0LjcxNDEsLTIuNzIxMjEgMTAuNDI1ODcsLTIuNjI2IDEyLjY4MjQyLDAuMjA2NjkgbCAxNC4yODg3NywxNy45MDI2OCBjIDE1LjU3NjE0LC02LjI1NjE4IDMyLjA0MTcyLC0xMC43MzE4NCA0OS4xNzg2NSwtMTMuMTYzMzUgbCAzLjQyOTk5LC0yMi42NjQ3OSBjIDAuNTQwMzQsLTMuNTg1NDIgNS40MzgzNCwtNi41MjIyNyAxMC44ODQwMSwtNi41MjIyNyBoIDMxLjY1MzU2IGMgNS40MzkxNiwwIDEwLjM1MzQ0LDIuOTM2ODUgMTAuODgwNzYsNi41MjIyNyBsIDMuNDIwMjIsMjIuNjY0NzkgYyAxNy4xNDM0NCwyLjQzMTUxIDMzLjYxMjI3LDYuOTA3MTcgNDkuMTkwODYsMTMuMTYzMzUgbCAxNC4yNzkwMSwtMTcuOTAyNjggYyAyLjI2NjMxLC0yLjgzMjY5IDcuOTc1NjQsLTIuOTI3OSAxMi42Nzk5NywtMC4yMDY2OSBsIDI3LjQyOTM0LDE1LjgzMDAzIGMgNC43MTA4NSwyLjcxNzk1IDcuNDg5MDIsNy43MDQ2NiA2LjE2MDk2LDExLjA5Mzk2IGwgLTguMzg5MDMsMjEuMzczMzUgYyAxMy4zNzY1NiwxMC41MDg4NyAyNS40NjE2OCwyMi42MDEzMiAzNS45ODAzMSwzNS45NzcwNiBsIDIxLjM3NDE4LC04LjM4MjUyIGMgMy4zNzg3MSwtMS4zMzQ1NyA4LjM2Mjk5LDEuNDMzODUgMTEuMDkzMTQsNi4xNDc5NCBsIDE1LjgzMDA0LDI3LjQyNjA4IGMgMi43Mjc3MSw0LjcxNDEgMi42MjI3NCwxMC40MTY5MiAtMC4yMTI0LDEyLjY3OTk4IGwgLTE3Ljg5NjE3LDE0LjI5NTI4IGMgNi4yNTIxMiwxNS41NjU1NyAxMC43Mjc3OCwzMi4wMzUyMSAxMy4xNTM2LDQ5LjE3Nzg1IGwgMjIuNjcwNDgsMy40MjY3MyBjIDMuNTkxOTIsMC41NDAzNCA2LjUxNjU3LDUuNDQyNDIgNi41MjYzMywxMC44NzEgdiAzMS42NzMwOCBjIDAsNS40NDU2NyAtMi45MzQ0MSwxMC4zNDA0MyAtNi41MjYzMywxMC44NjUzIGwgLTIyLjY3MDQ4LDMuNDIzNDggYyAtMi40MjU4MiwxNy4xNDI2MiAtNi45MDE0OCwzMy42MTU1MyAtMTMuMTUzNiw0OS4xOTA4NSBsIDE3Ljg5NjE3LDE0LjI5NzczIGMgMi44MzUxMywyLjI0NzYgMi45MzAzNSw3Ljk1MzY3IDAuMjEyNCwxMi42Njc3NyBsIC0xNS44MzAwNCwyNy40MjI4MyBjIC0yLjczMDE2LDQuNzE0MSAtNy43MTQ0Myw3LjQ5NTUyIC0xMS4wOTMxNCw2LjE2NjY2IGwgLTIxLjM3NDE4LC04LjM4MjUyIGMgLTEwLjUxODYzLDEzLjM4MzA2IC0yMi42MDM3NSwyNS40NTI3MiAtMzUuOTgwMzEsMzUuOTgzNTYgbCA4LjM4OTAzLDIxLjM2NzY2IGMgMS4zMjgwNiwzLjM3ODcyIC0xLjQ1MDExLDguMzY5NSAtNi4xNjA5NiwxMS4wODc0NSBsIC0yNy40MTk1NywxNS44MzU3MyBjIC00LjcxNDEsMi43MTc5NSAtMTAuNDIzNDMsMi42MTIxNiAtMTIuNjg5NzQsLTAuMjE5NzIgbCAtMTQuMjc5MDEsLTE3LjkwNDMgYyAtMTUuNTc4NTksNi4yNjE4NyAtMzIuMDQ3NDIsMTAuNzUxMzYgLTQ5LjE5MDg2LDEzLjE2ODIzIGwgLTMuNDIwMjIsMjIuNjYzMTUgYyAtMC41MjczMiwzLjU4ODY3IC01LjQ0MTYsNi41MTgyIC0xMC44NzA5OSw2LjUxODIgbCAtMzEuNjYzMzMsMC4wMjQ0IGMgLTUuNDQ1NjcsLTAuMDI0NCAtMTAuMzQzNjcsLTIuOTUzOTUgLTEwLjg4NDAxLC02LjU0MjYyIgogICAgICAgICBzdHlsZT0iZmlsbDojMDIyOTRkO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDowLjgxMzc1Nzk2IgogICAgICAgICBpZD0icGF0aDI2IgogICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPjwvZz48L2c+PC9zdmc+';

	return $menu;
}
add_filter( 'repo_manager_menu', 'repo_manager_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function repo_manager_settings_tabs( $tabs ) {
	$tabs['settings'] = __( 'Settings', 'repo-manager' );
	$tabs['support']  = __( 'Support', 'repo-manager' );

	return $tabs;
}
add_filter( 'repo_manager_settings_tabs', 'repo_manager_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function repo_manager_registered_settings_sections( $sections ) {
	$sections = array(
		'settings' => apply_filters(
			'repo_manager_settings_sections_settings',
			array(
				'main'    => __( 'General Settings', 'repo-manager' ),
				'strings' => __( 'String Settings', 'repo-manager' ),
			)
		),
		'support'  => apply_filters( 'repo_manager_settings_sections_support', array() ),
	);

	return $sections;
}
add_filter( 'repo_manager_registered_settings_sections', 'repo_manager_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function repo_manager_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'repo_manager_unsavable_tabs', 'repo_manager_define_unsavable_tabs' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function repo_manager_registered_settings( $settings ) {
	$new_settings = array(
		// General Settings.
		'settings' => apply_filters(
			'repo_manager_settings_settings',
			array(
				'main' => array(
					array(
						'id'   => 'settings_header',
						'name' => __( 'General Settings', 'repo-manager' ),
						'desc' => '',
						'type' => 'header',
					),
				),
			)
		),
		'support'  => apply_filters(
			'repo_manager_settings_support',
			array(
				array(
					'id'   => 'support_header',
					'name' => __( 'Open Repo Manager Support', 'repo-manager' ),
					'desc' => '',
					'type' => 'header',
				),
				array(
					'id'   => 'system_info',
					'name' => __( 'System Info', 'repo-manager' ),
					'desc' => '',
					'type' => 'sysinfo',
				),
			)
		),
	);

	return array_merge( $settings, $new_settings );
}
add_filter( 'repo_manager_registered_settings', 'repo_manager_registered_settings' );
